package com.makeIt.steadyAim.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makeIt.steadyAim.R;
import com.makeIt.steadyAim.model.Enterprise;

@SuppressLint({ "ViewHolder", "InflateParams" })
public class EnterpriseAdapter extends BaseAdapter {

	private final List<Enterprise> enterprises;
	private final Activity activity;
	
	public EnterpriseAdapter(List<Enterprise> enterprises, Activity activity) {
		super();
		this.enterprises = enterprises;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		return enterprises.size();
	}

	@Override
	public Object getItem(int position) {
		return enterprises.get(position);
	}

	@Override
	public long getItemId(int position) {
		return enterprises.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Enterprise enterprise = enterprises.get(position);
		LayoutInflater layoutInflater = activity.getLayoutInflater();
		
		View linha = layoutInflater.inflate(R.layout.enterprise_linha, null);
		
		TextView name = (TextView) linha.findViewById(R.id.enterprise_name);
		name.setText(enterprise.getName());
		
		return linha;
	}

}

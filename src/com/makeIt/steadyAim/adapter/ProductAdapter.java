package com.makeIt.steadyAim.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makeIt.steadyAim.R;
import com.makeIt.steadyAim.model.Product;

@SuppressLint({ "ViewHolder", "InflateParams" }) 
public class ProductAdapter extends BaseAdapter{

	private final List<Product> products;
	private final Activity activity;
	
	public ProductAdapter(List<Product> products, Activity activity) {
		super();
		this.products = products;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		return products.size();
	}

	@Override
	public Object getItem(int position) {
		return products.get(position);
	}

	@Override
	public long getItemId(int position) {
		return products.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Product product = products.get(position);
		LayoutInflater layoutInflater = activity.getLayoutInflater();
		
		View linha = layoutInflater.inflate(R.layout.product_linha, null);
		
		TextView name = (TextView) linha.findViewById(R.id.product_name);
		name.setText(product.getName());
		
		return linha;
	}

}

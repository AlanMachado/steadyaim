package com.makeIt.steadyAim.task;

import android.os.AsyncTask;

import com.makeIt.steadyAim.util.WebClient;

public class SimplesTask extends AsyncTask<Long, Double, String>{

	private String url;
	
	public SimplesTask(String url) {
		this.url = url;
	}


	@Override
	protected String doInBackground(Long... params) {
		alteraUrlConformeParams(params);
		WebClient client = new WebClient(url);
		return client.execute();
	}
	
	protected void alteraUrlConformeParams(Long... params){
		if(params.length > 0){
			for (Long param : params) {
				url = url.replace("?", param.toString());
			}
		}
	}
}

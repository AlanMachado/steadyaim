package com.makeIt.steadyAim.task;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.makeIt.steadyAim.R;
import com.makeIt.steadyAim.adapter.EnterpriseAdapter;
import com.makeIt.steadyAim.model.Enterprise;
import com.makeIt.steadyAim.util.WebClient;

public class EnterprisesTask extends AsyncTask<Integer, Double, String>{

	private final Activity context;
	
	public EnterprisesTask(Activity context) {
		this.context = context;
	}

	@Override
	protected String doInBackground(Integer... params) {
		WebClient client = new WebClient("http://192.177.1.105:8080/archeryService/enterprises/listAll");
		return client.execute();
	}
	
	@Override
	protected void onPostExecute(String result) {
		try {
			ListView lista = (ListView) context.findViewById(R.id.enterprises);
			String response = this.get();
			
			Type listType = new TypeToken<List<Enterprise>>(){}.getType();
			Gson gson = new Gson();
			List<Enterprise> enterprises = gson.fromJson(response, listType);
		
			EnterpriseAdapter adapter = new EnterpriseAdapter(enterprises, context);
			lista.setAdapter(adapter);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
	}

}

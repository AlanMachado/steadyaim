package com.makeIt.steadyAim;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.makeIt.steadyAim.adapter.EnterpriseAdapter;
import com.makeIt.steadyAim.listener.ListEnterpriseListener;
import com.makeIt.steadyAim.model.Enterprise;
import com.makeIt.steadyAim.task.SimplesTask;
import com.makeIt.steadyAim.util.URLServer;


public class EnterpriseActivity extends Activity {

	private ListView listEnterprises;
	private EnterpriseAdapter adapter;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enterprise_layout);
        processamento();
        
        listEnterprises.setOnItemClickListener(new ListEnterpriseListener(adapter,this));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.head, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    private void processamento(){
    	try {
			listEnterprises = (ListView) findViewById(R.id.enterprises);
			SimplesTask task = new SimplesTask(URLServer.enterprise_list_all);
		    String result = task.execute().get();
			
			Type listType = new TypeToken<List<Enterprise>>(){}.getType();
			Gson gson = new Gson();
			List<Enterprise> enterprises = gson.fromJson(result, listType);
		
			adapter = new EnterpriseAdapter(enterprises, this);
			listEnterprises.setAdapter(adapter);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
    }
}
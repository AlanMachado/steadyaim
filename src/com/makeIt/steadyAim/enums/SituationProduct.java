package com.makeIt.steadyAim.enums;

public enum SituationProduct {
	CONTINUED("continued"),
	DESCONTINUED("descontinued");
	
	private String value;

	private SituationProduct(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

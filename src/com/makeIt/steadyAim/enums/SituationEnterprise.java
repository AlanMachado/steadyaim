package com.makeIt.steadyAim.enums;

public enum SituationEnterprise {
	ACTIVE("active"),
	DISABLED("disabled");
	
	private String value;

	private SituationEnterprise(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

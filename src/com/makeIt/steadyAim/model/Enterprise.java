package com.makeIt.steadyAim.model;

import java.io.Serializable;
import java.util.List;

import com.makeIt.steadyAim.enums.SituationEnterprise;

@SuppressWarnings("serial")
public class Enterprise implements Serializable{

	
	private Long id;
	private String name;
	private SituationEnterprise situation;
	private List<Product> products;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	public SituationEnterprise getSituation() {
		return situation;
	}
	public void setSituation(SituationEnterprise situation) {
		this.situation = situation;
	}
	
	
	
}

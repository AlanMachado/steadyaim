package com.makeIt.steadyAim.model;

import com.makeIt.steadyAim.enums.SituationProduct;

public class Product {

	private Long id;
	private String name;
	private Enterprise manufacturerId;
	private Integer year;
	private SituationProduct situation;
	
	
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public SituationProduct getSituation() {
		return situation;
	}
	public void setSituation(SituationProduct situation) {
		this.situation = situation;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Enterprise getManufacturerId() {
		return manufacturerId;
	}
	public void setManufacturerId(Enterprise manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	
	
}

package com.makeIt.steadyAim;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.makeIt.steadyAim.adapter.ProductAdapter;
import com.makeIt.steadyAim.listener.ListProductListener;
import com.makeIt.steadyAim.model.Enterprise;
import com.makeIt.steadyAim.model.Product;
import com.makeIt.steadyAim.task.SimplesTask;
import com.makeIt.steadyAim.util.URLServer;

public class ProductActivity extends Activity {

	private ListView listProduct;
	private ProductAdapter adapter;
	private Enterprise enterprise;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_layout);
		
		Intent intent = getIntent();
		enterprise = (Enterprise) intent.getSerializableExtra("enterprise");
		
		processamento();
		
		listProduct.setOnItemClickListener(new ListProductListener(adapter, this));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.product, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void processamento(){
		try {
			listProduct = (ListView) findViewById(R.id.products);
			SimplesTask task = new SimplesTask(URLServer.product_by_enterprises);
			String result = task.execute(enterprise.getId()).get();
			
			Type listType = new TypeToken<List<Product>>(){}.getType();
			Gson gson = new Gson();
			List<Product> products = gson.fromJson(result, listType);
			
			adapter = new ProductAdapter(products, this);
			listProduct.setAdapter(adapter);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
}

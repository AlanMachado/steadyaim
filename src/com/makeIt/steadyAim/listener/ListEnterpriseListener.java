package com.makeIt.steadyAim.listener;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.makeIt.steadyAim.ProductActivity;
import com.makeIt.steadyAim.adapter.EnterpriseAdapter;
import com.makeIt.steadyAim.model.Enterprise;

public class ListEnterpriseListener implements OnItemClickListener{

	private EnterpriseAdapter adapter;
	private Activity context;
	
	public ListEnterpriseListener(EnterpriseAdapter adapter, Activity context) {
		this.adapter = adapter;
		this.context = context;
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Enterprise enterprise = (Enterprise) adapter.getItem(position);
		Intent goToProducts = new Intent(context,ProductActivity.class);
		goToProducts.putExtra("enterprise", enterprise);
		context.startActivity(goToProducts);
	}

}

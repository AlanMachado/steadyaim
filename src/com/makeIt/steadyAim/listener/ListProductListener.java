package com.makeIt.steadyAim.listener;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.makeIt.steadyAim.adapter.ProductAdapter;
import com.makeIt.steadyAim.model.Product;

public class ListProductListener implements OnItemClickListener {

	private ProductAdapter adapter;
	private Activity context;
	
	public ListProductListener(ProductAdapter adapter, Activity context) {
		this.adapter = adapter;
		this.context = context;
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Product product = (Product) adapter.getItem(position);
		Toast.makeText(context, "Item : " + product.getId() + " Nome: " + product.getName(), Toast.LENGTH_LONG).show();
	}
}
